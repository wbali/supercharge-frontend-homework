import { Component, OnInit } from '@angular/core';
import words from '../../hangman_words.json';
import { Utils } from '../../shared/utils';
import { environment } from '../../../environments/environment';

export enum GamePhase {
  START = 'START',
  IN_PROGRESS = 'IN_PROGRESS',
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  GamePhase = GamePhase;
  public wordList: string[] = words;
  public wordsByLength: { [length: number]: string[] } = {};
  public wordLengths: number[] = [];
  public gamePhase: GamePhase = GamePhase.START;
  public wordToGuess: string = '';

  constructor() {}

  ngOnInit(): void {
    this.wordList.forEach((value) => {
      if (this.wordsByLength.hasOwnProperty(value.length)) {
        const words = this.wordsByLength[value.length];
        words.push(value);
        this.wordsByLength = {
          ...this.wordsByLength,
          [value.length]: words,
        };
      } else {
        this.wordsByLength = {
          ...this.wordsByLength,
          [value.length]: [value],
        };
      }
    });
    this.wordLengths = Object.keys(this.wordsByLength).map((l) => +l);

    if (localStorage.getItem(environment.storageKey)) {
      this.gamePhase = GamePhase.IN_PROGRESS;
    }
  }

  onStartGame(wordCount: number) {
    this.gamePhase = GamePhase.IN_PROGRESS;
    const wordsForLength = this.wordsByLength[wordCount];
    this.wordToGuess =
      wordsForLength[
        Utils.getRandomNumberBetweenRange(0, wordsForLength.length - 1)
      ];
  }

  onStartNewGame() {
    this.gamePhase = GamePhase.START;
  }
}
