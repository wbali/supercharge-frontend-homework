import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectWordCountComponent } from './select-word-count.component';

describe('SelectWordCountComponent', () => {
  let component: SelectWordCountComponent;
  let fixture: ComponentFixture<SelectWordCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectWordCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectWordCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
