import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Utils } from '../../../shared/utils';

@Component({
  selector: 'app-select-word-count',
  templateUrl: './select-word-count.component.html',
  styleUrls: ['./select-word-count.component.scss'],
})
export class SelectWordCountComponent implements OnInit {
  @Input() wordLengths: number[] = [];

  @Output() start = new EventEmitter<number>();

  public selectedWordCount: number = 0;
  public randomSelected = false;

  constructor() {}

  ngOnInit(): void {}

  selectWordCount(wordCount: number) {
    this.randomSelected = false;
    this.selectedWordCount = wordCount;
  }

  selectRandomWordCount() {
    this.randomSelected = true;
  }

  startGame() {
    this.start.emit(
      this.randomSelected
        ? this.wordLengths[
            Utils.getRandomNumberBetweenRange(0, this.wordLengths.length - 1)
          ]
        : this.selectedWordCount
    );
  }
}
