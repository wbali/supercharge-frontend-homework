import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-in-progress',
  templateUrl: './in-progress.component.html',
  styleUrls: ['./in-progress.component.scss'],
})
export class InProgressComponent implements OnInit {
  @Input() wordToGuess: string = '';

  @Output() startNewGame = new EventEmitter<void>();

  public readonly INCORRECT_LETTER_LIMIT = 10;

  public alphabet: string[] = 'abcdefghijklmnopqrstuvwxyz'.split('');
  public gameInProgress: boolean = true;
  public chosenLetters: string[] = [];
  public lettersToGuess: string[] = [];
  public lettersGuessedInWord: string[] = [];
  public numberOfFailedGuesses: number = 0;
  public lettersGuessed: number = 0;
  public showWord = false;

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(event: KeyboardEvent) {
    if (
      this.alphabet.includes(event.key) &&
      !this.chosenLetters.includes(event.key)
    ) {
      this.chooseLetter(event.key);
    }
  }

  constructor() {}

  ngOnInit(): void {
    const savedProgress = localStorage.getItem(environment.storageKey);
    if (savedProgress) {
      this.gameInProgress = true;
      this.loadProgress(savedProgress);
    } else {
      this.lettersToGuess = this.wordToGuess.split('');
      this.lettersGuessedInWord = Array(this.wordToGuess.length).fill('');
    }
  }

  chooseLetter(letter: string) {
    if (this.gameInProgress) {
      this.chosenLetters.push(letter);
      if (this.lettersToGuess.includes(letter)) {
        this.lettersGuessedInWord = this.lettersToGuess.reduce(
          (acc, curr, i) => {
            if (curr === letter && !acc[i]) {
              acc[i] = letter;
              this.lettersGuessed += 1;
            }
            return acc;
          },
          this.lettersGuessedInWord
        );
      } else {
        this.numberOfFailedGuesses += 1;
      }

      if (this.lettersGuessed === this.lettersToGuess.length) {
        this.gameInProgress = false;
        this.removeProgress();
        return;
      }

      if (this.numberOfFailedGuesses === this.INCORRECT_LETTER_LIMIT) {
        this.gameInProgress = false;
        this.removeProgress();
        return;
      }

      this.saveProgress();
    }
  }

  endGame() {
    this.gameInProgress = false;
    this.showWord = true;
    this.lettersGuessedInWord = [...this.lettersToGuess];
    this.removeProgress();
  }

  newGame() {
    this.startNewGame.emit();
    this.removeProgress();
  }

  private loadProgress(progressString: string) {
    const parsed = JSON.parse(progressString);
    this.chosenLetters = parsed['chosenLetters'];
    this.lettersToGuess = parsed['lettersToGuess'];
    this.lettersGuessedInWord = parsed['lettersGuessedInWord'];
    this.numberOfFailedGuesses = parsed['numberOfFailedGuesses'];
    this.lettersGuessed = parsed['lettersGuessed'];
  }

  private saveProgress() {
    localStorage.setItem(
      environment.storageKey,
      JSON.stringify({
        chosenLetters: this.chosenLetters,
        lettersToGuess: this.lettersToGuess,
        lettersGuessedInWord: this.lettersGuessedInWord,
        numberOfFailedGuesses: this.numberOfFailedGuesses,
        lettersGuessed: this.lettersGuessed,
      })
    );
  }

  private removeProgress() {
    localStorage.removeItem(environment.storageKey);
  }

  isLetterDisabled(letter: string): boolean {
    return this.chosenLetters.includes(letter);
  }
}
